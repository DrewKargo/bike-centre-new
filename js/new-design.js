document.addEventListener("DOMContentLoaded", function() { 
    
    const body = document.querySelector(".body"),
          infoButtons = document.querySelectorAll(".header-main__info-item"),
          header = document.querySelector(".header"),
          headerTop = document.querySelector(".header-top"),
          headerMain = document.querySelector(".header-main"),
          headerBot = document.querySelector(".header-bot");
          
          
    
    let lastscroll = 0;
    window.addEventListener("scroll", () => {
        if(window.scrollY > lastscroll && window.scrollY > headerTop.scrollHeight){
            headerBot.classList.add("header-bot_hidden");
            infoButtons.forEach((btn) => btn.classList.remove("hidden"));
            
        }else{
                setTimeout(() => {
                    headerTop.style="margin-bottom: 0";
                    infoButtons.forEach((btn) => btn.classList.add("hidden"));
                },50);
            if (window.scrollY < (lastscroll + headerBot.scrollHeight)){
                headerBot.classList.remove("header-bot_hidden");
            }
        }
        lastscroll = window.scrollY;
    }); 
    function closeModalByEsc(currentModal){
        if(currentModal.classList.contains("modal-open")){
            closeModal(currentModal);
        }
    }

    document.addEventListener("keydown", (e) =>{
        if(e.key === "Escape"){
            if(searchModal.classList.contains("modal-open")){
            closeModalByEsc(searchModal);
            } else if(menuModal.classList.contains("modal-open")){
                burgerIcon.classList.toggle("close");
                closeModal(menuModal);
                body.classList.remove("no-scroll");
            }
        }
    });
    
    // Блок поиска
    
    
    const searchBlock = document.querySelector(".search-block"),
          searchContainer = document.querySelector(".search-block__wrapper"),
          searchInput = document.querySelector(".search-block__input"),
          searchClearBtn = document.querySelector(".search-block__btn_clear"),
          searchBtn = document.querySelector(".search-block__btn"),
          searchBtnText = document.querySelector(".search-block__btn-text"),
          searchShowBtn = document.querySelector(".header-main__search-btn_mobile"),
          searchModal = document.querySelector(".search-block__modal");
    
    function openModal(element){
        element.classList.remove("modal-close");
        element.classList.add("modal-open");
    }
    function closeModal(element){
        element.classList.remove("modal-open");
        element.classList.add("modal-close");
    }
    
    document.addEventListener("click", (e) => {
        if(!searchContainer.contains(e.target) && searchBlock.classList.contains('active')){
            searchBlock.classList.remove("active");
            closeModal(searchModal);
        }
    });
    window.addEventListener("resize", () => {
        if(window.innerWidth > 767 && searchContainer.classList.contains("modal-close")){
            searchContainer.classList.remove("modal-close");
            searchBtnText.textContent="Найти";
            closeModal(searchModal);
        }
        if(window.innerWidth > 767 && searchModal.classList.contains("modal-open")){
            searchBtnText.textContent="Найти";
        }
        if(window.innerWidth < 768 && searchModal.classList.contains("modal-open")){
            closeModal(searchModal);
            searchBtnText.textContent="Отмена";
        }
    });
    
    searchInput.addEventListener("focus", ()=>{
        searchBlock.classList.add("active");
        if(searchInput.value){
            openModal(searchModal);
        }
    });
    
    searchContainer.addEventListener("focusout", ()=>{
        if(searchBlock.classList.contains("active") && !searchModal.classList.contains("modal-open")){
            searchBlock.classList.remove("active");
        }
    });
    
    searchInput.addEventListener("input", ()=>{
        searchBlock.classList.add("active");
        if(searchInput.value){
            searchClearBtn.classList.add("active");
            openModal(searchModal);
        }
        else{
            searchClearBtn.classList.remove("active");
        }
    });
    
    
    searchClearBtn.addEventListener("click", (e) =>{
        e.preventDefault();
        searchInput.value='';
        searchClearBtn.classList.remove("active");
    })
    searchBtn.addEventListener("click", (e) =>{
        if(window.innerWidth < 768){
            e.preventDefault();
            closeModal(searchContainer);
        }
    });
    searchShowBtn.addEventListener("click", () =>{
        searchBtnText.textContent="Отмена";
        openModal(searchContainer);
        openModal(searchModal);
    })
    
    document.addEventListener("keydown", (e) =>{
        if(e.key === "Enter"){
            if(searchInput.value.length > 2 && searchBlock.classList.contains("active")){
                searchBlock.submit();
            }
        }
    });
    
    // Блок кнопки аккаунта
    
    
    const accountBtn = document.querySelector(".header-main__account-btn"),
          accountModalMenu = document.querySelector(".header-main__account-menu"),
          accountContainer = document.querySelector(".header-main__account-container");
    
    
    accountContainer.addEventListener("mouseleave", (e) =>{
    
            if(e.target !== accountBtn || e.target !== accountModalMenu){
                
                closeModal(accountModalMenu);
            }
        
    });
    
    accountBtn.addEventListener("mouseover", () =>{
        openModal(accountModalMenu);
    });
    
    // Блок меню
    
    
    const burgerBtn = document.querySelector(".burger-btn"),
          burgerIcon = document.querySelector(".burger-btn__icon"),
          menuModal = document.querySelector(".menu-modal");
    
    
    burgerBtn.addEventListener("click", () => {
        burgerIcon.classList.toggle("close");
        if(burgerIcon.classList.contains("close")){
            openModal(menuModal);
            body.classList.add("no-scroll");
            if(searchModal.classList.contains("modal-open")){
                if(headerBot.classList.contains("header-bot_hidden")){
                    menuModal.style=`top:${header.scrollHeight - searchModal.scrollHeight}px; bottom:0`;
                } else {
                    menuModal.style=`top:${header.scrollHeight - searchModal.scrollHeight + headerBot.scrollHeight}px; bottom:0`;
                }

            } else {
                if(window.scrollY < 1){
                    menuModal.style=`top:${header.scrollHeight}px; bottom:0`;
                }else{
                    menuModal.style=`top:${header.scrollHeight - headerTop.scrollHeight}px; bottom:0`;
                }
            }
            showSubcategoriesOfTheActiveCategory(currentCategory);
            removeCatalogActiveCategories(currentCategory);
            renderMasonryItems(currentCategory);
        } else {
           closeModal(menuModal);
           body.classList.remove("no-scroll");
        }
        
    });

    window.addEventListener("resize", () => {
        if(window.innerWidth < 993 && menuModal.classList.contains("modal-open")){
        }
        closeModal(menuModal);
        burgerIcon.classList.remove("close")
        body.classList.remove("no-scroll");
    });
    
    // Блок мобильного меню
    
    
    const mobileTabContainer = document.querySelector(".menu-mobile__sections-wrapper"),
          mobileTabBtns = document.querySelectorAll(".menu-mobile__tabs-item"),
          mobileTabsToggle = document.querySelector(".menu-mobile__tabs-toggle"),
          mobileTabCatalog = document.querySelector(".menu-mobile__tabs-item_catalog"),
          mobileTabBrands = document.querySelector(".menu-mobile__tabs-item_brands");
    
    
    mobileTabBtns.forEach((el) => {
        el.addEventListener("click", (e) => {
            
            if(e.target == mobileTabCatalog){
                mobileTabBrands.classList.remove("menu-mobile__tabs-item_active");
                e.target.classList.add("menu-mobile__tabs-item_active");
                mobileTabsToggle.style="transform:translateX(0)";
                mobileTabContainer.style="transform:translateX(0)";
            } 
            else if(e.target == mobileTabBrands){
                mobileTabCatalog.classList.remove("menu-mobile__tabs-item_active");
                e.target.classList.add("menu-mobile__tabs-item_active");
                mobileTabsToggle.style="transform:translateX(100%)";
                mobileTabContainer.style="transform:translateX(-50%)";
            }
        })
    });
    
    
    // Блок брендов мобильного меню
    
    
    const brandLetterTitle = document.querySelector(".menu-mobile__brands-title_letter"),
          brandLetterBtns = document.querySelectorAll(".menu-mobile__brands-letter-slide"),
          brandsItems = document.querySelectorAll(".menu-mobile__brands-item"),
          basicVuewLetter = "A";;
          
    
    brandLetterTitle.textContent = basicVuewLetter;
    
    function showCurrentBrandsByLetter(letter){
        brandsItems.forEach((item) => {
            item.classList.remove("menu-mobile__brands-item_active");
            if(item.getAttribute("data-brand-letter") == letter){
              item.classList.add("menu-mobile__brands-item_active");
            }
        });
    }
    
    showCurrentBrandsByLetter(basicVuewLetter);
    
    
    brandLetterBtns.forEach((btn) => {
        btn.addEventListener("click", () => {
            brandLetterBtns.forEach((btn) => btn.classList.remove("active"));
            btn.classList.add("active");
            let currentBrandLetter = btn.getAttribute("data-brands-letter");
            brandLetterTitle.textContent = `${currentBrandLetter}`;
            showCurrentBrandsByLetter(currentBrandLetter);
        });
    });
    
    const mobileBrandsSlider = new Swiper(".menu-mobile__brands-slider", {
        spaceBetween: 8,
        lazyLoading : true,
        loop: true,
        slidesPerView: "auto",
        freeMode: true,
    });
    
    const mobileBrandsLettersSlider = new Swiper(".menu-mobile__brands-letter-slider", {
        spaceBetween: 12,
        lazyLoading : true,
        slidesPerView: "auto",
        freeMode: true,
      });
    
    
    // Блок мобильного каталога
    
    const catalogItems = document.querySelectorAll(".catalog-mobile__item"),
          subcategoriesTitle = document.querySelector(".catalog-mobile__subcategories-title"),
          subcategoriesList = document.querySelector(".catalog-mobile__subcategories-list"),
          backToCategoriesBtn = document.querySelector(".catalog-mobile__btn-back"),
          dropWindowSubsategories = document.querySelector(".catalog-mobile__navigation-window_subcategories");

        
    let subcategoriesItems,
        getSubcategoriesItems = () => {
            return subcategoriesItems = document.querySelectorAll(".catalog-mobile__subcategories-item");
        };
    
    
    catalogItems.forEach((item) => {
        item.addEventListener("click", () => {
            while(subcategoriesList.firstChild){
                subcategoriesList.firstChild.remove();
            }
            if(item.hasAttribute("title")){
                sendSubcategoryToNavigationWindow(item);
                getSubcategoriesItems();
                openSubcategoryNavigationWindow();
            }
            dropWindowSubsategories.classList.add("catalog-mobile__navigation-window_active");
            
            sendingTitleToSubsection(subcategoriesTitle, item);
            
        })
    });

    function sendingTitleToSubsection(changedTitle, currentCategoryItem){
        if(currentCategoryItem.hasAttribute("title")){
            let category = currentCategoryItem.getAttribute("title");
            changedTitle.innerHTML = category;
        }
    }

    const sendSubcategoryToNavigationWindow = (category) => {
        
        catalogSubcategoriesSections.forEach((section) => {

            section.classList.remove("menu-main__subcategories-section_active");
            
            if(category.getAttribute("title") == section.getAttribute("data-subcategory")){
                [...section.children].forEach(element => {

                    let linkHref = element.firstElementChild.getAttribute("href"),
                        elementTitle = element.firstElementChild.getAttribute("title"),
                        newItem = document.createElement("li")
                        typesContainer = element.querySelector(".subcategory__list-types"),
                        itemContainer = document.createElement("div");

                    itemContainer.classList.add("catalog-mobile__subcategories-container");
                    newItem.classList.add("catalog-mobile__subcategories-item");

                    newItem.appendChild(itemContainer); 
                    itemContainer.insertAdjacentHTML('beforeEnd', `<a class="catalog-mobile__subcategories-link" href="${linkHref}" title="${elementTitle}">${elementTitle}</a>`);
                    itemContainer.insertAdjacentHTML('beforeEnd', `<svg class="right" width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#chevron-right"></use>
                    </svg>`);
                    
                    // Условие позволяет проверить наличие типов в typesContainer
                    if(typesContainer){

                        // Если у субкатегории есть типы присваивается атрибут, позволяющий предотвратить переход по ссылке и открыть всплывающее окно с типами
                        newItem.setAttribute("data-hassubcategory", true);

                        let newTypesContainer = document.createElement("ul"),
                            subcategoryTypesList = typesContainer.querySelectorAll(".subcategory__types-title");

                        subcategoryTypesList.forEach((item) => {
                            let linkHref = item.getAttribute("href"),
                                elementTitle = item.getAttribute("title"),
                                newTypeItem = document.createElement("li");

                            newTypeItem.classList.add("catalog-mobile__subcategories-item");
                            

                            newTypesContainer.appendChild(newTypeItem);
                            newTypeItem.insertAdjacentHTML('beforeEnd', `<a class="catalog-mobile__subcategories-link" href="${linkHref}" title="${elementTitle}">${elementTitle}</a>`);
                            newTypeItem.insertAdjacentHTML('beforeEnd', `<svg class="right" width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="#chevron-right"></use>
                            </svg>`);

                        });
                        let newDropWindowTypes = document.createElement("div");
                        newDropWindowTypes.classList.add("catalog-mobile__navigation-window","catalog-mobile__navigation-window_types", "catalog-mobile__subcategories_types")
                        newDropWindowTypes.innerHTML = `
                            <div class="catalog-mobile__subcategories-head">
                                <h3 class="catalog-mobile__type-title">${elementTitle}</h3>
                                <button id="btn-back-types" class="catalog-mobile__btn-back">
                                    <img src="./images/icons/mobile-categories-arrow-back.svg" alt="Назад">
                                </button>
                            </div>
                        `;
                        newDropWindowTypes.appendChild(newTypesContainer);

                        let backBtn = newDropWindowTypes.querySelector(".catalog-mobile__btn-back");
                        backBtn.addEventListener("click", () =>{
                            closeNavigationWindow(backBtn);
                        })
                        newItem.appendChild(newDropWindowTypes);

                    } else {
                        newItem.setAttribute("data-hassubcategory", false);
                    }

                    subcategoriesList.insertAdjacentElement('beforeEnd', newItem);
                    
                  });
            } 
        });
    }
    const openSubcategoryNavigationWindow = () => {
        subcategoriesItems.forEach((item) => {
            let dropWindowTypes = item.querySelector(".catalog-mobile__subcategories_types");
            
            item.addEventListener("click", (e) => {
                
                if(item.hasAttribute("data-hassubcategory") && item.getAttribute("data-hassubcategory") == "true"){
                    // Аттрибут data-hassubcategory="true", добавленный к элементу списка саталога предотваращет переход по ссылке, вложенной в этот элемент.
                   
                    if(e.target.closest(".catalog-mobile__subcategories-container")){

                        e.preventDefault();

                        // И показывает  всплывающее подменю раздела.
                        dropWindowTypes.classList.add("catalog-mobile__navigation-window_active");

                    }
                }
                
            });
        });
    }

    function closeNavigationWindow(button){

        button.classList.add("btn-back-animation");
        setTimeout(() => {
            button.classList.remove("btn-back-animation");
        }, 300);
        
        if(button.getAttribute("id") == "btn-back-types"){
            let typesNavigationWindow = document.querySelectorAll(".catalog-mobile__subcategories_types");

            typesNavigationWindow.forEach((typeWindow) => {
                if(typeWindow.classList.contains("catalog-mobile__navigation-window_active")){

                    typeWindow.classList.remove("catalog-mobile__navigation-window_active");
        
                }
            });
        } else {
            dropWindowSubsategories.classList.remove("catalog-mobile__navigation-window_active");
        }
    }

    backToCategoriesBtn.addEventListener("click", (e) => {
        if(backToCategoriesBtn.contains(e.target)){
            closeNavigationWindow(backToCategoriesBtn);
        }
    });
    
    
    // Блок католога меню
    
    const currentCategory = document.querySelector(".menu-main__subcategories-section_active"),
          catalogCategoriesItems = document.querySelectorAll(".catalog-main__item")
          catalogSubcategoriesSections = document.querySelectorAll(".menu-main__subcategories-section");
    
    const renderMasonryItems = (section) => {

        const msnry = new Masonry( section,{
            itemSelector: '.subcategory',
            columnWidth: '.subcategory',
            percentPosition: true,
            gutter: 100,
        });
    }
    renderMasonryItems(currentCategory);

    let removeCatalogActiveCategories = (current) => {

        catalogCategoriesItems.forEach((item) => { item.classList.remove("catalog-main__item_active")});
        catalogCategoriesItems[0].classList.add("catalog-main__item_active");
        current.classList.add("menu-main__subcategories-section_active");
    };
    
    catalogCategoriesItems.forEach((item) => {

        item.addEventListener("mouseover", (e) => {

            catalogCategoriesItems.forEach((item) => { item.classList.remove("catalog-main__item_active")});

            if(e.target == item.hasAttribute("title") || e.target.closest(".catalog-main__item")){
                item.classList.add("catalog-main__item_active");
                showSubcategoriesOfTheActiveCategory(item);
            } 
        })
    });

    const showSubcategoriesOfTheActiveCategory = (category) => {
        
        catalogSubcategoriesSections.forEach((section) => {

            section.classList.remove("menu-main__subcategories-section_active");
            if(category.classList.contains("catalog-main__item_active") && category.getAttribute("title") == section.getAttribute("data-subcategory")){
                section.classList.add("menu-main__subcategories-section_active");
            } 
            renderMasonryItems(section);
        });
    }

    window.addEventListener("resize", () => {

        showSubcategoriesOfTheActiveCategory(currentCategory);
    });
    
});
